<?php

use yii\db\Migration;

class m160911_200224_defaultData extends Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }


        $this->addPosNDep($tableOptions);
        $this->addRole($tableOptions);
        $this->addUser($tableOptions);
    }

    public function addPosNDep($tableOptions) {
        $this->insert('{{%department}}', [
            'id' => 1,
            'name' => 'IT',
            'hod_id' => 1,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
                ], $tableOptions);

        $this->insert('{{%position}}', [
            'id' => 1,
            'position' => 'General Manager',
            'department_id' => 1,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
                ], $tableOptions);
    }

    public function addRole($tableOptions) {
        $this->insert('{{%role}}', [
            'id' => 1,
            'name' => 'Administrator',
            'description' => 'This role can control all',
            'created_at' => new \yii\db\Expression('CURRENT_TIMESTAMP()'),
            'updated_at' => new \yii\db\Expression('CURRENT_TIMESTAMP()'),
                ], $tableOptions);
        $this->insert('{{%role}}', [
            'id' => 2,
            'name' => 'Staff',
            'description' => 'This role can alter and view data',
            'created_at' => new yii\db\Expression('NOW()'),
            'updated_at' => new yii\db\Expression('NOW()'),
                ], $tableOptions);
        $this->insert('{{%role}}', [
            'id' => 3,
            'name' => 'Student',
            'description' => 'This role can only view data',
            'created_at' => new yii\db\Expression('NOW()'),
            'updated_at' => new yii\db\Expression('NOW()'),
                ], $tableOptions);
    }

    public function addUser($tableOptions) {

        $this->insert('{{%user}}', [
            'id' => 1,
            'username' => 'Admin1',
            'staff_no' => 'staff123',
            'ic_no' => '901231-01-1234',
            'phone_no' => '012-34567890',
            'email' => 'admin1@mail.com',
            'name' => 'Nama Staff 1',
            'staff_employer_id' => 1,
            'position_id' => 1,
            'role_id' => 1,
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_hash' => Yii::$app->security->generatePasswordHash('admin1'),
            'created_at' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
            'updated_at' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
                ], $tableOptions);
    }

    public function down() {
//        $this->truncateTable('{{%role}}');
//        $this->delete('{{%role}}', ['id' => 1]);
//        $this->delete('{{%role}}', ['id' => 2]);
//        $this->delete('{{%role}}', ['id' => 3]);
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
