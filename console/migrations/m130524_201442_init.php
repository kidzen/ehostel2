<?php

use yii\db\Migration;

class m130524_201442_init extends Migration {

    //normalize for Foreign Key Name : <fk>_<this table name>_<refference table name>_<refference column name>
    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'role_id' => $this->integer()->notNull()->defaultValue(0),  //need to change
            'email' => $this->string()->notNull()->unique(),
            'name' => $this->string(),
            'alias' => $this->string(),
            'position_id' => $this->integer(),
            'staff_employer_id' => $this->integer(),
            'staff_no' => $this->string(),
            'ic_no' => $this->string(),
            'phone_no' => $this->string(),
            'picture' => $this->string()->defaultValue('tiada_gambar.jpg'),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'status' => $this->smallInteger()->defaultValue(1), //needto change
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'last_log' => $this->integer(),
                ], $tableOptions);
    }

    public function down() {
        $this->dropTable('{{%user}}');
    }

}
