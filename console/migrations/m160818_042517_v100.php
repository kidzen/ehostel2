<?php

use yii\db\Migration;

class m160818_042517_v100 extends Migration {

    //normalize for Foreign Key Name : <fk>_<this table name>_<refference table name>_<refference column name>
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%student}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'matrix_no' => $this->string()->notNull()->unique(),
            'ic_no' => $this->string()->notNull()->unique(),
            'phone_no' => $this->string(),
            'email' => $this->string()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
                ], $tableOptions);
        $this->createTable('{{%hostel}}', [
            'id' => $this->primaryKey(),
            'no_hostel' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
                ], $tableOptions);
        $this->createTable('{{%bilik}}', [
            'id' => $this->primaryKey(),
            'hostel_id' => $this->integer()->notNull(),
            'no_bilik' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
                ], $tableOptions);

        $this->createIndex('bilik_uk', '{{%bilik}}', ['id', 'hostel_id'],true);
        $this->addForeignKey('fk_bilik_hostel_id', '{{%bilik}}', 'hostel_id', '{{%hostel}}', 'id', 'cascade', 'cascade');

        $this->createTable('{{%address}}', [
            'id' => $this->primaryKey(),
            'address_for_user' => $this->smallInteger()->notNull()->defaultValue(0), //     1:user ,      2:student 
            'user_id' => $this->integer()->notNull(), //user or student id
            'address' => $this->string(),
            'poskod' => $this->integer(5),
            'state' => $this->string(),
            'country' => $this->string(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
                ], $tableOptions);

        $this->createTable('{{%student_bilik}}', [
            'id' => $this->primaryKey(),
            'student_id' => $this->integer()->notNull(), //user or student id
            'bilik_id' => $this->integer()->notNull(), //user or student id
            'date_register' => $this->date(),
            'valid_until' => $this->date(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
                ], $tableOptions);
        $this->addForeignKey('fk_student_bilik_student_id', '{{%student_bilik}}', 'student_id', '{{%student}}', 'id');
        $this->addForeignKey('fk_student_bilik_bilik_id', '{{%student_bilik}}', 'bilik_id', '{{%bilik}}', 'id');

        $this->createTable('{{%role}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'description' => $this->string(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
                ], $tableOptions);
        $this->addForeignKey('fk_user_role_id', '{{%user}}', 'role_id', '{{%role}}', 'id');

        $this->createTable('{{%position}}', [
            'id' => $this->primaryKey(),
            'position' => $this->string()->notNull(),
            'department_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
                ], $tableOptions);

        $this->addForeignKey('fk_user_position_id', '{{%user}}', 'position_id', '{{%position}}', 'id');

        $this->createTable('{{%department}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'hod_id' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
                ], $tableOptions);

        $this->addForeignKey('fk_position_department_id', '{{%position}}', 'department_id', '{{%department}}', 'id');
    }

    public function safeDown() {
        $this->dropForeignKey('fk_bilik_hostel_id', '{{%bilik}}');
        $this->dropForeignKey('fk_student_bilik_student_id', '{{%student_bilik}}');
        $this->dropForeignKey('fk_student_bilik_bilik_id', '{{%student_bilik}}');
        $this->dropForeignKey('fk_user_role_id', '{{%user}}');
        $this->dropForeignKey('fk_user_position_id', '{{%user}}');
        $this->dropForeignKey('fk_position_department_id', '{{%position}}');

        $this->dropTable('{{%student}}');
        $this->dropTable('{{%hostel}}');
        $this->dropTable('{{%bilik}}');
        $this->dropTable('{{%address}}');
        $this->dropTable('{{%student_bilik}}');
        $this->dropTable('{{%role}}');
        $this->dropTable('{{%position}}');
        $this->dropTable('{{%department}}');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
