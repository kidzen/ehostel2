<?php

namespace common\helpers;

use kartik\helpers\Enum as baseEnum;

class Enum extends baseEnum {

//    
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    
    public static $status = [
        'inactive' => 0,
        'active' => 1,
        'approved' => 2,
        'pending' => 3,
        'deleted' => 4,
        'new' => 5,
        'blocked' => 7,
        'rejected' => 8,
        'disposed' => 9,
    ];

}

 