<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Bilik;

/**
 * BilikSearch represents the model behind the search form about `common\models\Bilik`.
 */
class BilikSearch extends Bilik {

    /**
     * @inheritdoc
     */
    public function attribute() {

        return array_merge(parent::attributes(), [
            'hostel.no_hostel',
        ]);
    }

    public function rules() {
        return [
            [['id', 'hostel_id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['no_bilik', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Bilik::find();
        $query->joinWith('hostel');
        if (!Yii::$app->user->isAdmin) {
            $query->andWhere(['bilik.status' => 0]);
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
//        $dataProvider->sort->defaultOrder = ['ID' => SORT_DESC];
//        $dataProvider->sort->attributes['hostel.no_hostel'] = ['asc' => ['hostel.no_hostel' => SORT_ASC], 'desc' => ['hostel.no_hostel' => SORT_DESC],];


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'hostel_id' => $this->hostel_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'no_bilik', $this->no_bilik]);
//        $query->andFilterWhere(['like', 'hostel.no_hostel', $this->getAttribute('hostel.no_hostel')]);

        return $dataProvider;
    }

}
