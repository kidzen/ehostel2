<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\UserProfile;

/**
 * This is the model class for table "bilik".
 *
 * @property integer $id
 * @property integer $hostel_id
 * @property string $no_bilik
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Hostel $hostel
 * @property StudentBilik[] $studentBiliks
 */
class Bilik extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;
    const SCENARIO_SINGLE_ROOM = 'single-room';
    const SCENARIO_MULTIPLE_ROOM = 'multiple-room';

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'bilik';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['hostel_id', 'no_bilik'], 'required', 'except' => self::SCENARIO_SINGLE_ROOM],
//            [['id', 'hostel_id', 'no_bilik'], 'required'],
            [['hostel_id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['no_bilik'], 'string', 'max' => 255],
            [['hostel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hostel::className(), 'targetAttribute' => ['hostel_id' => 'id']],
            // a1 needs to be unique
//        ['a1', 'unique'],
            // a1 needs to be unique, but column a2 will be used to check the uniqueness of the a1 value
//        ['hostel_id', 'unique', 'targetAttribute' => 'no_bilik'],
            // a1 and a2 need to be unique together, and they both will receive error message
//        [['hostel_id', 'no_bilik'], 'unique', 'targetAttribute' => ['no_bilik', 'hostel_id']],
            // a1 and a2 need to be unique together, only a1 will receive error message
            ['no_bilik', 'unique', 'targetAttribute' => ['hostel_id', 'no_bilik'], 'message' => 'already exist'],
                // a1 needs to be unique by checking the uniqueness of both a2 and a3 (using a1 value)
//        ['a1', 'unique', 'targetAttribute' => ['a2', 'a1' => 'a3']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'hostel_id' => Yii::t('app', 'Hostel ID'),
            'no_bilik' => Yii::t('app', 'No Bilik'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHostel() {
        return $this->hasOne(Hostel::className(), ['id' => 'hostel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentBiliks() {
        return $this->hasMany(StudentBilik::className(), ['bilik_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(UserProfile::className(), ['id' => 'created_by']);
    }

    public function getUpdator() {
        return $this->hasOne(UserProfile::className(), ['id' => 'updated_by']);
    }

}
