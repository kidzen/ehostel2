<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\UserProfile;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property integer $role_id
 * @property string $email
 * @property string $name
 * @property string $alias
 * @property integer $position_id
 * @property integer $staff_employer_id
 * @property string $staff_no
 * @property string $ic_no
 * @property string $phone_no
 * @property string $picture
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $last_log
 *
 * @property Position $position
 * @property Role $role
 */
class UserProfile extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public $password;
    public $repassword;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('UNIX_TIMESTAMP()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['username', 'email', 'auth_key', 'password_hash', 'role_id'], 'required', 'on' => 'sign-in'],
            [['username', 'email', 'password', 'repassword', 'role_id'], 'required', 'on' => 'create'],
            [['username', 'email', 'auth_key', 'password_hash', 'role_id'], 'required', 'on' => 'update'],
            [['role_id', 'position_id', 'staff_employer_id', 'status', 'created_at', 'updated_at', 'last_log'], 'integer'],
            [['username', 'email', 'name', 'alias', 'staff_no', 'ic_no', 'phone_no', 'picture', 'password_hash', 'password_reset_token', 'password', 'repassword'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['position_id'], 'exist', 'skipOnError' => true, 'targetClass' => Position::className(), 'targetAttribute' => ['position_id' => 'id']],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['role_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'role_id' => Yii::t('app', 'Role ID'),
            'email' => Yii::t('app', 'Email'),
            'name' => Yii::t('app', 'Name'),
            'alias' => Yii::t('app', 'Alias'),
            'position_id' => Yii::t('app', 'Position ID'),
            'staff_employer_id' => Yii::t('app', 'Staff Employer ID'),
            'staff_no' => Yii::t('app', 'Staff No'),
            'ic_no' => Yii::t('app', 'Ic No'),
            'phone_no' => Yii::t('app', 'Phone No'),
            'picture' => Yii::t('app', 'Picture'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password' => Yii::t('app', 'Password'),
            'repassword' => Yii::t('app', 'Repeat Password'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'last_log' => Yii::t('app', 'Last Log'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition() {
        return $this->hasOne(Position::className(), ['id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole() {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(UserProfile::className(), ['id' => 'created_by']);
    }

    public function getUpdator() {
        return $this->hasOne(UserProfile::className(), ['id' => 'updated_by']);
    }

}
