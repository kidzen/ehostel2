<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\UserProfile;

/**
 * This is the model class for table "hostel".
 *
 * @property integer $id
 * @property string $no_hostel
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Bilik[] $biliks
 */
class Hostel extends \yii\db\ActiveRecord {

    public $no_of_room;
    public $multiple_insert;
    public $room_no;

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;
    const SCENARIO_MULTIPLE_ROOM = 'multiple-room';
    const SCENARIO_SINGLE_ROOM = 'single-room';
    const SCENARIO_WITH_ROOM = 'with-room';

//    public function attributes() {
//        return array_merge(parent::attributes(), [
//            'multiple_insert',
//        ]);
//    }

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'hostel';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['no_hostel'], 'required'],
            [['no_of_room'], 'required',
//                'when' => function ($model) { return $model->multiple_insert == true; },
                'whenClient' => "MultipleInsert_no_of_room", 'on' => self::SCENARIO_WITH_ROOM
            ],
            [['room_no'], 'required',
////                'when' => function ($model) { return $model->multiple_insert == false; },
                'whenClient' => "MultipleInsert_room_no", 'on' => self::SCENARIO_WITH_ROOM
            ],
            [['status', 'created_by', 'updated_by', 'no_of_room', 'room_no', 'multiple_insert'], 'integer'],
            [['room_no'], 'string', 'max' => 255],
//            [['room_no'], 'exist', 'skipOnError' => true, 'targetClass' => Bilik::className(), 'targetAttribute' => ['room_no' => 'no_bilik']],
            [['created_at', 'updated_at'], 'safe'],
            [['no_hostel'], 'string', 'max' => 255],
            [['no_hostel'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'no_hostel' => Yii::t('app', 'No Hostel'),
            'no_of_room' => Yii::t('app', 'Number Of Room'),
            'multiple_insert' => Yii::t('app', 'Create multiple room'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBiliks() {
        return $this->hasMany(Bilik::className(), ['hostel_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(UserProfile::className(), ['id' => 'created_by']);
    }

    public function getUpdator() {
        return $this->hasOne(UserProfile::className(), ['id' => 'updated_by']);
    }

}
