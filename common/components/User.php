<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

class User extends \yii\web\User {

    public function getName() {
        $identity = $this->getIdentity();

        return $identity !== null ? $identity->getName() : 'Guest';
    }

    public function getJoinDate() {
        $identity = $this->getIdentity();

//        return gmdate("Y-m-d\TH:i:s\Z", $timestamp);
//        return date("d-m-Y\TH:i:s\Z", $timestamp);

        return $identity !== null ? date("M. Y", $identity->getJoinDate()) : null;
    }

    public function getAccess() {
        $identity = $this->getIdentity();

        return $identity !== null ? $identity->getAccessLevel() : null;
    }

    public function getRoleName() {
        $identity = $this->getIdentity();

        return $identity !== null ? $identity->getRoleName() : 'Guest';
    }

    public function getIsSuperAdmin() {
        $identity = $this->getAccess();

        return $identity == 0 ? true : false;
    }

    public function getIsAdmin() {
        $identity = $this->getAccess();

        return $identity == 1 ? true : false;
    }

    public function getIsStaff() {
        $identity = $this->getAccess();

        return $identity == 2 ? true : false;
    }

    public function getIsStudent() {
        $identity = $this->getAccess();

        return $identity == 3 ? true : false;
    }

//    public function getIsManager() {
//        $identity = $this->getAccess();
//
//        return $identity == 4 ? true : false;
//    }

//    public function getIsDeveloper() {
//        $identity = $this->getAccess();
//
//        return $identity == 5 ? true : false;
//    }

//    public function getIdentity($autoRenew = true) {
//        if ($this->_identity === false) {
//            if ($this->enableSession && $autoRenew) {
//                $this->_identity = null;
//                $this->renewAuthStatus();
//            } else {
//                return null;
//            }
//        }
//
//        return $this->_identity;
//    }
}
