<?php
//var_dump(Yii::$app->user->id); 
//    die();
    
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

//isset(Yii::$app->user->identity->roles->NAME)? Yii::$app->user->identity->roles->NAME : null;
//isset(Yii::$app->user->identity->roles->NAME)? Yii::$app->user->identity->roles->NAME : null;
$username = Yii::$app->user->name ? Yii::$app->user->name :'Guest';
$activeBool = Yii::$app->user->id ? 'Online' : 'Offline';
$roles = Yii::$app->user->roleName;


if (class_exists('backend\assets\AppAsset')) {
    backend\assets\AppAsset::register($this);
} else {
    frontend\assets\AppAsset::register($this);
}


//dmstr\web\AdminLteAsset::register($this);
frontend\assets\AdminLteAsset::register($this);
//frontend\assets\AppAsset::register($this);
//AdminLteAsset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@frontend/assets/dist-adminlte');
//$directoryAsset = Yii::$app->assetManager->getPublishedUrl('');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link rel="shortcut icon" href="<?php echo $directoryAsset; ?>/img/favicon.ico" type="image/x-icon" />
        <?php $this->head() ?>
    </head>
    <!--<body class="hold-transition skin-purple-light sidebar-mini">-->
    <body class="hold-transition skin-purple-light fixed sidebar-mini">
        <?php $this->beginBody() ?>
        <div class="wrapper">

            <?=
            $this->render(
                    'header.php', [
                        'directoryAsset' => $directoryAsset,
                        'username' => $username,
                        'activeBool' => $activeBool,
                        'roles' => $roles,
                    ]
            )
            ?>

            <?=
            $this->render(
                    'left.php', [
                        'directoryAsset' => $directoryAsset,
                        'username' => $username,
                        'activeBool' => $activeBool,
                        'roles' => $roles,
                    ]
            )
            ?>

            <?=
            $this->render(
                    'content.php', [
                        'content' => $content, 
                        'directoryAsset' => $directoryAsset,
                        'username' => $username,
                        'activeBool' => $activeBool,
                        'roles' => $roles,
                    ]
            )
            ?>

        </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
