<?php

use yii\helpers\Url;
?>

<aside class="main-sidebar">

    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">

            <?php if (Yii::$app->user->id) { ?>
                <div class="user-panel center-block">
                    <img src="<?= $directoryAsset ?>/img/tiada_gambar.jpg" class="img-bordered img-responsive img-circle center-block" alt="user_pic"/>

                    <h3 class="text-center"><?= $username ?></h3>
                    <h5 class="text-center"><?= $roles ?></h5>
                    <!--<img src="<?= $directoryAsset ?>/img/tiada_gambar.jpg" class="img-circle img-bordered img-responsive"  alt="user_pic"/>-->
                    <!--<img src="<?= $directoryAsset ?>/img/tiada_gambar.jpg" class="img-circle center-block" style="width: 160px" alt="user_pic"/>-->

                </div>
            <?php } else { ?>
                <div class="user-panel center-block">
                    <img src="<?= $directoryAsset ?>/img/sistem.png" class="img-bordered img-responsive img-circle center-block" alt="sistem_logo"/>

                    <h3 class="text-center"><?= $username ?></h3>
                    <h5 class="text-center"><?= $roles ?></h5>
                    <!--<img src="<?= $directoryAsset ?>/img/tiada_gambar.jpg" class="img-circle img-bordered img-responsive"  alt="user_pic"/>-->
                    <!--<img src="<?= $directoryAsset ?>/img/tiada_gambar.jpg" class="img-circle center-block" style="width: 160px" alt="user_pic"/>-->

                </div>
            <?php } ?>
        </div>

        <!-- search form -->
        <!--                <form action="#" method="get" class="sidebar-form">
                            <div class="input-group">
                                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                              <span class="input-group-btn">
                                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                            </div>
                        </form>-->
        <!-- /.search form -->
        <div style="overflow: true">
            <?=
            dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu delay'],
                        'items' => [
//                            ['label' => '', 'options' => ['class' => 'header']],
                            ['label' => 'Dashboard', 'icon' => 'fa fa-home', 'url' => ['site/index']],
                            [
                                'label' => 'e-Hostel',
                                'icon' => 'fa fa-building',
                                'style' => 'color:blue;',
                                //'visible' => !Yii::$app->user->isGuest,
                                'url' => '#',
                                'items' => [
                                    [
                                        'label' => 'Management', 'icon' => 'fa fa-gear', 'url' => '#',
                                        'items' => [
                                            ['label' => 'Hostel Management', 'icon' => 'fa fa-file-o', 'url' => ['hostel-management/index'],
                                                'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isGuest,],
                                            ['label' => 'Room Management', 'icon' => 'fa fa-file-o', 'url' => ['hostel-management/room'],
                                                'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isGuest,],
                                        ],
                                    ],
                                    [
                                        'label' => 'Students', 'icon' => 'fa fa-gear', 'url' => '#',
                                        'items' => [
                                            ['label' => 'Registration', 'icon' => 'fa fa-file-o', 'url' => ['student/create'],
                                                'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isGuest,],
                                            ['label' => 'All Student', 'icon' => 'fa fa-users', 'url' => ['student/all-student-record'],
                                                'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isGuest,],
                                        ],
                                    ],
                                    ['label' => 'Room Assignment', 'icon' => 'fa fa-file-code-o', 'url' => ['room-assignment/check-in'],
                                        'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isGuest,],
                                    ['label' => 'Student Room List', 'icon' => 'fa fa-file-code-o', 'url' => ['room-assignment/index'],
                                        'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isGuest,],
                                ],
                            ],
                            [
                                'label' => 'Pentadbiran Sistem',
                                'icon' => 'fa fa-gear',
                                'style' => 'color:blue;',
                                //'visible' => !Yii::$app->user->isGuest,
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Roles', 'icon' => 'fa fa-unlock-alt', 'url' => ['role/index'],
                                        'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isGuest,],
                                    ['label' => 'Student', 'icon' => 'fa fa-user-md', 'url' => ['student/index'],
                                        'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isGuest,],
                                    ['label' => 'Users', 'icon' => 'fa fa-users', 'url' => ['user-profile/index'],
                                        'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isGuest,],
                                    ['label' => 'My Profile', 'icon' => 'fa fa-user', 'url' => ['user-profile/view', 'id' => Yii::$app->user->id],],
                                ],
                            ],
                            ['label' => 'Login', 'icon' => 'fa fa-sign-in', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                            [
                                'label' => 'Developer tools',
                                'icon' => 'fa fa-share',
                                // 'visible' => Yii::$app->user->isAdmin,
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
                                    ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
                                    [
                                        'label' => 'Database',
                                        'icon' => 'fa fa-share',
                                        //       'visible' => Yii::$app->user->isAdmin,
                                        'url' => '#',
                                        'items' => [
//                                            ['label' => 'Users', 'icon' => 'fa fa-cubes', 'url' => ['user-profile/index']],
//                                            ['label' => 'Roles', 'icon' => 'fa fa-cubes', 'url' => ['role/index']],
//                                            ['label' => 'Address', 'icon' => 'fa fa-cubes', 'url' => ['address/index']],
//                                            ['label' => 'Department', 'icon' => 'fa fa-cubes', 'url' => ['department/index']],
//                                            ['label' => 'Position', 'icon' => 'fa fa-cubes', 'url' => ['position/index']],
                                            ['label' => 'Hostel', 'icon' => 'fa fa-cubes', 'url' => ['hostel/index']],
//                                            ['label' => 'Bilik', 'icon' => 'fa fa-cubes', 'url' => ['bilik/index']],
                                            ['label' => 'Student Bilik', 'icon' => 'fa fa-cubes', 'url' => ['student-bilik/index']],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ]
            )
            ?>
        </div>
    </section>


    <!--            <div class="slimScrollBar" style="width: 3px; position: absolute; top: 31px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 169.656px; background: rgba(0, 0, 0, 0.2);">
                </div>
                <div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(51, 51, 51);">
                </div>
                </div>-->
</aside>
