<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UserProfile */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Profiles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info user-profile-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>User Profile : <?=  Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">
    <p>
        <?= Html::a(Yii::t('app', 'Back'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
        'method' => 'post',
        ],
        ]) ?>
    </p>
    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                [
            'attribute'=>'id',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'username',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'role_id',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'email',
            'format'=>'email',
            'vAlign' => 'middle',
            ],
            [
            'attribute'=>'name',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'alias',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'position_id',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'staff_employer_id',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'staff_no',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'ic_no',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'phone_no',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'picture',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'auth_key',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'password_hash',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'password_reset_token',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'status',
                        'vAlign' => 'middle',
            'value' => $model->status <= 0 ? 'Inactive' : 'Active',
            ],
            [
            'attribute'=>'created_at',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'updated_at',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'last_log',
                        'vAlign' => 'middle',
            ],
    ],
    ]) ?>

</div>
