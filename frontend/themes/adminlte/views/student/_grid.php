<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

$script = <<< JS
$(document).ready(function() {
    setInterval(function(){ $("#refreshButton").click(); }, 1000);
});
JS;
$this->registerJs($script);
?>

<?php Pjax::begin(['id' => 'time']); ?>     
<?= Html::a("Refresh", ['create'], ['id' => 'refreshButton', 'class' => 'hidden btn btn-lg btn-primary']) ?>
<?= 'Current time : ' . $time; ?>
<?php Pjax::end(); ?>
<?php Pjax::begin(['id' => 'grid_student']); ?>     
<?=

GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
    'pjax' => true, // pjax is set to always true for this demo
    // parameters from the demo form
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    //        'showPageSummary' => true,
    'responsiveWrap' => false,
    'persistResize' => false,
    //        'exportConfig' => $exportConfig,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        //[
        //      'attribute'=>'id',
        //                  //      'hAlign' => 'center', 'vAlign' => 'middle',
        //],
        [
            'attribute' => 'name',
            'hAlign' => 'center', 'vAlign' => 'middle',
        ],
        [
            'attribute' => 'matrix_no',
            'hAlign' => 'center', 'vAlign' => 'middle',
        ],
        [
            'attribute' => 'ic_no',
            'hAlign' => 'center', 'vAlign' => 'middle',
        ],
        [
            'attribute' => 'phone_no',
            'hAlign' => 'center', 'vAlign' => 'middle',
        ],
        [
            'attribute' => 'email',
            'format' => 'email',
            'hAlign' => 'center', 'vAlign' => 'middle',
        ],
        [
            'attribute' => 'status',
            'class' => 'kartik\grid\BooleanColumn',
            'trueIcon' => '<span class="label label-success">ACTIVE</span>',
            'falseIcon' => '<span class="label label-danger">INACTIVE</span>',
            'falseLabel' => 'INACTIVE',
            'trueLabel' => 'ACTIVE',
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isSuperAdmin,
            'template' => '{view} {update} {delete} {recover}',
            'viewOptions' => ['title' => 'View', 'data-toggle' => 'tooltip'],
            'updateOptions' => ['title' => 'Update', 'data-toggle' => 'tooltip'],
            'buttons' => [
                'recover' => function ($url, $model) {
                    if ($model->status === 0) {
                        return Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, ['title' => Yii::t('yii', 'Recover'), 'data-toggle' => 'tooltip',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to RECOVER this item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',]);
                    }
                },
                        'delete' => function ($url, $model) {
                    if ($model->status === 1) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'), 'data-toggle' => 'tooltip',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to DELETE this item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',]);
                    }
                },
                    ],
                ],
                [
                    'header' => 'Permanent Delete',
                    'class' => 'kartik\grid\ActionColumn',
                    'template' => '{delete-permanent}',
                    'buttons' => [
                        'delete-permanent' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash" style="color:red;"></span>', $url, [
                                        'title' => Yii::t('yii', 'Permanent Delete'), 'data-toggle' => 'tooltip',
                                        'data-confirm' => Yii::t('yii', 'Are you sure you want to PERMANENTLY DELETE this item? This action cannot be UNDO!'),
                                        'data-method' => 'post',
                                        'data-pjax' => '0',
                            ]);
                        }
                            ],
                            'visible' => Yii::$app->user->isAdmin,
                        ],
                    ],
                ]);
                ?>
                <?php Pjax::end(); ?>
