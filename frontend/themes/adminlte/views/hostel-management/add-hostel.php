<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Hostel */

$this->title = Yii::t('app', 'Create Hostel');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Hostels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hostel-create">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
        </div>

        <?=
        $this->render('_form-hostel', [
            'model' => $model,
//            'modelItem' => $modelItem,
        ])
        ?>

    </div>
</div>
