<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Bilik */

$this->title = Yii::t('app', 'Create Bilik');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Biliks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bilik-create">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
        </div>

        <?= 
        $this->render('_form-room', [
            'model' => $model,
            'hostelArray' => $hostelArray,
        ])
        ?>

    </div>
</div>
