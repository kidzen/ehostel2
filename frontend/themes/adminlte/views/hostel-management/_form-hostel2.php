<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Hostel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hostel-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'no_hostel')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?php // echo $form->field($model, 'multiple_insert')->checkbox([0 => 'Create single room', 1 => 'Create multiple room in instance'],['onchange'=>'multipleInsertRoom(this);']) ?>
                <?= $form->field($model, 'multiple_insert')->checkbox([0 => 'Create single room', 1 => 'Create multiple room in instance'], ['class' => 'icheckbox_flat-green checked hover']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4" id="hostel-multiple-insert-room-fals">
                <?= $form->field($model, 'no_of_room')->textInput(['maxlength' => true,])->hint('Avoid more than 500 room added per transaction.') ?>
            </div>
            <div class="col-md-4" id="hostel-multiple-insert-room-tru">
                <?= $form->field($modelItem, 'no_bilik')->textInput(['maxlength' => true])->hint('Room no max to 999') ?>
            </div>

        </div>
        <div>
            <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
