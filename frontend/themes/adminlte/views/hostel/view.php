<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Hostel */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Hostels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info hostel-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>Hostel : <?=  Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">
    <p>
        <?= Html::a(Yii::t('app', 'Back'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
        'method' => 'post',
        ],
        ]) ?>
    </p>
    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                [
            'attribute'=>'id',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'no_hostel',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'status',
                        'vAlign' => 'middle',
            'value' => $model->status <= 0 ? 'Inactive' : 'Active',
            ],
            [
            'attribute'=>'created_at',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'updated_at',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'created_by',
                        'vAlign' => 'middle',
            ],
            [
            'attribute'=>'updated_by',
                        'vAlign' => 'middle',
            ],
    ],
    ]) ?>

</div>
