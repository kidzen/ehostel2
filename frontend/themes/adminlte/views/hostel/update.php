<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Hostel */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Hostel',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Hostels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="hostel-update">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
        </div>

        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>

    </div>
</div>
