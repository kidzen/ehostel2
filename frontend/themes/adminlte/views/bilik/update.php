<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Bilik */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Bilik',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Biliks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="bilik-update">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
        </div>

        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>

    </div>
</div>
