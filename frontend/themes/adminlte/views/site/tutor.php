
<?php
echo \yii\helpers\Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> Print', ['/site/tutor-pdf'], [
    'class' => 'btn btn-info',
    'target' => '_blank',
    'data-toggle' => 'tooltip',
    'title' => 'Will open the generated PDF file in a new window'
]);
?>
<div style="text-align:left;">
    <ol>
        <li>Role dalam sistem:-
            <ul>
                <li>Admininistrator : Developer Mode
                    <ul>
                        <li>No Restriction</li>
                    </ul>
                </li>
                <li>Pegawai Stor : Administrator Mode
                    <ul>
                        <li>Restriction on modify password</li>
                        <li>Restriction on creating roles</li>
                    </ul>
                </li>
                <li>Manager : Approver Mode</li>
                <ul>
                    <li>Allow view and order approval only</li>
                </ul>
        </li>
        <li>User : Able to request order</li>
        <ul>
            <li>Allow view and request order</li>
        </ul>
        </li>
        <li>Spectator : Able to see report only</li>
        </ul>
        </li>
        <br>
        <li>Bina elemen rujukan sistem:-
            <ul>
                <li>Users</li>
                <li>Categories</li>
                <li>Usage List</li>
                <li>Vendors</li>
                <li>Inventories</li>
            </ul>
        </li>
        <br>
        <li>Permohonan stok:-
            <ul>
                <li>1 barang untuk setiap permohonan</li>
                <li>1 permohonan borang baucer yang mempunyai lebih dari satu akan dipisahkan kepada beberapa permohonan</li>
                <li>Permohonan yang belum di sah kan dikira masih belum dikeluarkan dari stor</li>
            </ul>
        </li>
        <br>
        <li>Pengesahan stok:-
            <ul>
                <li>1 pengesahan untuk 1 barang dalam permohonan</li>
                <li>jenis pengesahan
                    <ul>
                        <li>Approve : permulaan kiraan stok & tidak boleh di `Disposed`. Stok yg telah di sahkan perlu di reject terlebih dahulu untuk di disposed.</li>
                        <li>Pending : trigger notifikasi pada Manager/Pegawai Stor</li>
                        <li>Rejected : jika sudah disahkan, reverse stok akan di lakukan.</li>
                        <li>Disposed : Tukar status kepada inactive</li>
                    </ul>
                </li>
            </ul>
        </li>
        <br>
        <li>Perhatian: Permanent delete hanya boleh dilakukan oleh admin dan pegawai stor. Option ini tidak boleh di undo!</li>
    </ol>
</div>
