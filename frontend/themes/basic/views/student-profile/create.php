<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StudentProfile */

$this->title = 'Create Student Profile';
$this->params['breadcrumbs'][] = ['label' => 'Student Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-profile-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
