<?php

namespace frontend\controllers;

use Yii;
use common\models\Student;
use common\models\StudentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\components\AccessRule;
use kartik\widgets\Alert;
use kartik\widgets\Growl;
use yii\base\Exception;
use common\helpers\Enum;

/**
 * StudentController implements the CRUD actions for Student model.
 */
class StudentController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-multiple' => ['POST'],
                    'delete-permanent' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),],
                'only' => ['create', 'update', 'index', 'view',
                    'delete', 'delete-permanent', 'recover'],
                'rules' => [
                    [
                        'actions' => ['delete-permanent'],
                        'allow' => true,
                        'roles' => ['Administrator'],
                    ],
                    [
                        'actions' => ['create', 'update', 'delete', 'recover'],
                        'allow' => true,
                        'roles' => ['Staff', 'Administrator'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['Student', 'Staff', 'Administrator'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Student models.
     * @return mixed
     */
    private function getTime() {
        $time = date('H:i:s', time());
        return $time;
    }

    public function actionAllStudentRecord() {
        $searchModel = new StudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('all-student-record', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Student model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findStudent($id),
        ]);
        //return $this->redirect(['index']);
    }

    /**
     * Creates a new Student model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $time = $this->getTime();
        $model = new Student();
        $searchModel = new StudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                \Yii::$app->notify->success(' Item created.');
            } else {
                \Yii::$app->notify->fail(' Item create fail.');
                return $this->render('create', [
                            'model' => $model,
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                ]);
            }
        }
        return $this->render('create', [
                    'time' => $time,
                    'model' => new Student(),
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing Student model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findStudent($id);
        $searchModel = new StudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                \Yii::$app->notify->success(' Item updated.');
//                return $this->redirect(['view', 'id' => $model->id]);
                //return $this->redirect(['index']);
            } else {
                \Yii::$app->notify->fail(' Item update fail.');
            }
        }
        return $this->render('update', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Deletes an existing Student model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findStudent($id);
        $model->status = Enum::STATUS_INACTIVE;
        if ($model->save()) {
            \Yii::$app->notify->success(' Item deleted.');
        } else {
            \Yii::$app->notify->fail(' Item cannot be deleted.');
        }
        return $this->redirect(\Yii::$app->request->referrer);
    }

    public function actionRecover($id) {
        $model = $this->findStudent($id);
        $model->status = Enum::STATUS_ACTIVE;
        if ($model->save()) {
            \Yii::$app->notify->success(' Item recovered.');
        } else {
            \Yii::$app->notify->fail(' Item cannot be recovered.');
        }
        return $this->redirect(\Yii::$app->request->referrer);
    }

    public function actionDeletePermanent($id) {
        if ($this->findStudent($id)->delete())
            \Yii::$app->notify->success(' Item permanently deleted.');
        else
            \Yii::$app->notify->fail(' Item cannot be permanently deleted.');

        return $this->redirect(['index']);
    }

    /**
     * Finds the Student model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Student the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findStudent($id) {
        if (($model = Student::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
