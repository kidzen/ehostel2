<?php

namespace frontend\controllers;

use Yii;
use common\models\StudentBilik;
use common\models\StudentBilikSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\components\AccessRule;
use kartik\widgets\Alert;
use kartik\widgets\Growl;
use yii\base\Exception;
use common\helpers\Enum;


/**
 * RoomAssignmentController implements the CRUD actions for StudentBilik model.
 */
class RoomAssignmentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-multiple' => ['POST'],
                    'delete-permanent' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),],
                'only' => ['create', 'update', 'index', 'view',
                    'delete', 'delete-permanent', 'recover'],
                'rules' => [
                    [
                        'actions' => ['delete-permanent'],
                        'allow' => true,
                        'roles' => ['Administrator'],
                    ],
                    [
                        'actions' => ['create', 'update', 'delete', 'recover'],
                        'allow' => true,
                        'roles' => ['Staff','Administrator'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['Student','Staff','Administrator'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all StudentBilik models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StudentBilikSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single StudentBilik model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findStudentBilik($id),
        ]);
        //return $this->redirect(['index']);
    }

    /**
     * Creates a new StudentBilik model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new StudentBilik();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                \Yii::$app->notify->success(' Item created.');
                return $this->redirect(['view', 'id' => $model->id]);
                //return $this->redirect(['index']);
            } else {
                \Yii::$app->notify->fail(' Item create fail.');
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing StudentBilik model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findStudentBilik($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                \Yii::$app->notify->success(' Item updated.');
                return $this->redirect(['view', 'id' => $model->id]);
                //return $this->redirect(['index']);
            } else {
                \Yii::$app->notify->fail(' Item update fail.');
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing StudentBilik model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findStudentBilik($id);
        $model->status = Enum::STATUS_INACTIVE;
        if($model->save()){
            \Yii::$app->notify->success(' Item deleted.');
        } else {
            \Yii::$app->notify->fail(' Item cannot be deleted.');
        }
        return $this->redirect(['index']);
    }

    public function actionRecover($id) {
        $model = $this->findStudentBilik($id);
        $model->status = Enum::STATUS_ACTIVE;
        if($model->save()){
            \Yii::$app->notify->success(' Item recovered.');
        } else {
            \Yii::$app->notify->fail(' Item cannot be recovered.');
        }
        return $this->redirect(['index']);
    }
    
    public function actionDeletePermanent($id)
    {
        if($this->findStudentBilik($id)->delete())
            \Yii::$app->notify->success(' Item permanently deleted.');
        else
            \Yii::$app->notify->fail(' Item cannot be permanently deleted.');        

        return $this->redirect(['index']);
    }

    /**
     * Finds the StudentBilik model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StudentBilik the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findStudentBilik($id)
    {
        if (($model = StudentBilik::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
