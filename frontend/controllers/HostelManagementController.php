<?php

namespace frontend\controllers;

use Yii;
use common\models\Bilik;
use common\models\BilikSearch;
use common\models\Hostel;
use common\models\HostelSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\components\AccessRule;
use kartik\widgets\Alert;
use kartik\widgets\Growl;
use yii\base\Exception;
use common\helpers\Enum;

class HostelManagementController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-multiple' => ['POST'],
//                    'delete-permanent' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),],
                'only' => ['create', 'update', 'index', 'view',
                    'delete', 'delete-permanent', 'recover'],
                'rules' => [
                    [
                        'actions' => ['delete-permanent'],
                        'allow' => true,
                        'roles' => ['Administrator'],
                    ],
                    [
                        'actions' => ['create', 'update', 'delete', 'recover'],
                        'allow' => true,
                        'roles' => ['Staff', 'Administrator'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['Student', 'Staff', 'Administrator'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new BilikSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $searchModel2 = new HostelSearch();
        $dataProvider2 = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'searchModel2' => $searchModel2,
                    'dataProvider2' => $dataProvider2,
        ]);
    }

    public function actionHostel() {
        $searchModel = new HostelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('hostel', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAddHostel() {

        $model = new Hostel(['scenario' => Hostel::SCENARIO_WITH_ROOM]);
//        $modelItem = new Bilik(['scenario' => Bilik::SCENARIO_SINGLE_ROOM]);

        if ($model->load(Yii::$app->request->post())) {
            $dbtransac = Yii::$app->db->beginTransaction();
            try {
                if ($model->multiple_insert) {
                    if (!$model->save()) {
                        throw new Exception('Hostel cannot be save.');
                    }
                    $maxRoomCounter = Bilik::find(['hostel_id' => $model->id])->max('no_bilik');
                    for ($i = 1; $i <= $model->no_of_room; $i++) {
                        $modelItems = new Bilik();
                        $modelItems->hostel_id = $model->id;
                        $modelItems->no_bilik = str_pad($i, 3, 0, STR_PAD_LEFT);
                        if (!$modelItems->save())
                            throw new Exception('Room cannot be created for respective hostel.');
                    }
                } else if (!$model->multiple_insert) {
                    if (!$model->save()) {
                        throw new Exception('Hostel cannot be save.');
                    }
                    $modelItem = new Bilik(['scenario' => Bilik::SCENARIO_SINGLE_ROOM]);
                    $modelItem->hostel_id = $model->id;
                    $modelItem->no_bilik = str_pad($model->room_no, 3, 0, STR_PAD_LEFT);
                    if (!$modelItem->save()) {
                        throw new Exception('Room cannot be created for respective hostel.');
                    }
                } else {
                    throw new Exception('Unexpected error');
                }

                $dbtransac->commit();
                Yii::$app->notify->success('Hostel added successfully.');
                return $this->redirect(['index']);
            } catch (Exception $e) {
                Yii::$app->notify->fail($e->getMessage(), 9000);
                $dbtransac->rollBack();
            }
        }
        return $this->render('add-hostel', [
                    'model' => $model,
//                    'modelItem' => $modelItem,
        ]);
    }

    public function actionAddHostel2() {

        $model = new Hostel();
        $modelItem = new Bilik(['scenario' => Bilik::SCENARIO_SINGLE_ROOM]);
//        $modelItem = Hostel::findOne(['status'=>1]);
//        var_dump($modelItem->addError($attribute, $error));die();

        if ($model->load(Yii::$app->request->post())) {
            $dbtransac = Yii::$app->db->beginTransaction();
            try {
                if ($model->multiple_insert) {
                    if (empty($model->no_of_room)) {
                        $model->addError('no_of_room', 'Number of room cannot be blank');
                        throw new Exception('Number of room cannot be blank.');
                    }
                    if (!$model->save()) {
                        throw new Exception('Hostel cannot be save.');
                    }
                    for ($i = 1; $i <= $model->no_of_room; $i++) {
                        $modelItems = new Bilik();
                        $modelItems->hostel_id = $model->id;
                        $modelItems->no_bilik = str_pad($i, 3, 0, STR_PAD_LEFT);
                        if (!$modelItems->save())
                            throw new Exception('Room cannot be created for respective hostel.');
                    }
                } else if (!$model->multiple_insert && $modelItem->load(Yii::$app->request->post())) {
                    if (empty($modelItem->no_bilik)) {
                        $modelItem->addError('no_bilik', 'Room Number cannot be blank');
                        throw new Exception('Room Number cannot be blank.');
                    }
                    if (!$model->save()) {
                        throw new Exception('Hostel cannot be save.');
                    }
                    $modelItem->hostel_id = $model->id;
                    $modelItem->no_bilik = str_pad($modelItem->no_bilik, 3, 0, STR_PAD_LEFT);
                    if (!$modelItem->save()) {
                        throw new Exception('Room cannot be created for respective hostel.');
                    }
                } else {
                    throw new Exception('Unexpected error');
                }
                $dbtransac->commit();
                Yii::$app->notify->success('Hostel added successfully.');
                return $this->redirect(['index']);
            } catch (Exception $e) {
                Yii::$app->notify->fail($e->getMessage(), 9000);
                $dbtransac->rollBack();
            }
        }
        return $this->render('add-hostel2', [
                    'model' => $model,
                    'modelItem' => $modelItem,
        ]);
    }

    public function actionAddRoom() {
        $model = new Bilik();
        $hostel = Hostel::findAll(['status' => Enum::STATUS_ACTIVE]);
        $hostelArray = \yii\helpers\ArrayHelper::map($hostel, 'id', 'no_hostel');

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->notify->success('Room added successfully.');
                return $this->redirect(['index']);
            } else {
                Yii::$app->notify->fail('Room cannot be add.');
            }
        }
        return $this->render('add-room', [
                    'model' => $model,
                    'hostelArray' => $hostelArray,
        ]);
    }

    public function actionDelete($id) {
        $dbtransac = Yii::$app->db->beginTransaction();
        $room = $this->findRoom($id);

        try {
            if (empty(Bilik::findAll(['hostel_id' => $room->hostel_id]))) {
                $hostel = $this->findHostel($room->hostel_id);
                $hostel->status = Enum::STATUS_INACTIVE;
                if (!$hostel->save(false)) {
                    throw new Exception('Respective hostel is empty, but cannot be deleted.');
                }
            } else {
                $room->status = Enum::STATUS_INACTIVE;
                if (!$room->save(false)) {
                    throw new Exception('Room cannot be deleted.');
                }
            }

            $dbtransac->commit();
            Yii::$app->notify->success('success');
        } catch (Exception $e) {
            $dbtransac->rollBack();
            Yii::$app->notify->fail($e->getMessage());
        }
        return $this->redirect(['index']);
    }

    public function actionRecover($id) {
        $dbtransac = Yii::$app->db->beginTransaction();
        $room = $this->findRoom($id);

        try {
            if (empty(Bilik::findAll(['hostel_id' => $room->hostel_id]))) {
                $hostel = $this->findHostel($room->hostel_id);
                $hostel->status = Enum::STATUS_INACTIVE;
                if (!$hostel->save(false)) {
                    throw new Exception('Respective hostel is empty, but cannot be deleted.');
                }
            } else {
                $room->status = Enum::STATUS_INACTIVE;
                if (!$room->save(false)) {
                    throw new Exception('Room cannot be deleted.');
                }
            }

            $dbtransac->commit();
            Yii::$app->notify->success('success');
        } catch (Exception $e) {
            $dbtransac->rollBack();
            Yii::$app->notify->fail($e->getMessage());
        }
        return $this->redirect(['index']);
    }

    public function actionDeletePermanent($id) {
        $dbtransac = Yii::$app->db->beginTransaction();
        $room = $this->findRoom($id);
        try {
            if ($room->status == Enum::STATUS_ACTIVE) {
                throw new Exception('Active item cannot be permanently deleted.');
            }
            if (empty(Bilik::findAll(['hostel_id' => $room->hostel_id]))) {
                if (!$this->findHostel($room->hostel_id)->delete()) {
                    throw new Exception('Respective hostel is empty, but cannot be deleted.');
                }
            } else {
                if (!$room->delete()) {
                    throw new Exception('Room cannot be deleted.');
                }
            }

            $dbtransac->commit();
            Yii::$app->notify->success('success');
        } catch (Exception $e) {
            $dbtransac->rollBack();
            Yii::$app->notify->fail($e->getMessage());
        }
        return $this->redirect(['index']);
    }

    protected function findRoom($id) {
        if (!empty($model = Bilik::findOne($id))) {
            return $model;
        } else {
            throw new NotFoundHttpException('Data does not exist.');
        }
    }

    protected function findHostel($id) {
        if (!empty($model = Hostel::findOne($id))) {
            return $model;
        } else {
            throw new NotFoundHttpException('Data does not exist.');
        }
    }

}
