/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//////////////////////////////////////////////////////////////////////////////////

/*
 * Validation function for Hostel model when insert multiple room
 * Required  for both no_of_room and room_no field
 */
function MultipleInsert_no_of_room(attribute, value) {
//    return $('#hostel-multiple_insert').is(':checked');
    return $('#hostel-multiple_insert').prop('checked');
}
function MultipleInsert_room_no(attribute, value) {
    return !$('#hostel-multiple_insert').prop('checked');
}

$(function () {
    /*
     * animation class for menu
     */
    var type = 'fadeInUp';
    var count = 0;
    $('.delay>li').each(function () {
        jQuery(this).addClass('animate' + count + ' ' + type);
        jQuery(this).on('animationend', function (e) {
            //jQuery(this).removeAttr('class');
        });
        count++;
    });
    var type = 'fadeInDown';
    var count1 = 0;
    $('.delay>.delay-child').each(function () {
        jQuery(this).addClass('animate' + count1 + ' ' + type);
        jQuery(this).on('animationend', function (e) {
            //jQuery(this).removeAttr('class');
        });
        count1++;
    });
//    return true;


    /*
     * Request Form Modal
     */
    $('.request').click(function () {
        $.get(
                "index.php?r=request/create-ajax",
                function (data) {
                    $("#modalRequest").modal('show')
                            .find('#modalRequestContent').html(data);
                }
        );
    });
    /*
     * Entry Form Modal
     */
    $('.entry-bulk').click(function () {
        $.get(
                "index.php?r=entry/create-bulk-ajax",
                function (data) {
                    $("#modalCreateBulk").modal('show')
                            .find('#modalCreateBulkContent').html(data);
                }
        );
    });

    /*
     * Tutor Page Modal
     */

    $('.tutorButton').click(function () {
        $.get(
                "index.php?r=site/tutor",
                function (data) {
                    $('#modalTutor').modal('show')
                            .find('#modalTutorContent')
//                            .load($(this).attr('value'));
                            .html(data);
                });
    });

    $('.view-button').click(function () {
        $('#modalView').modal('show')
                .find('#modalViewContent')
//                .load($(this).attr('href'));
                .load($(this).attr('value'));
//                .html(data);
    });

    /*
     * Hide and show no_of_room and room_no field for multiple insert hostel
     */
    $("#hostel-multiple_insert").change(function () {
        if ($(this).is(":checked")) {
            console.log('insert multiple room');
            $("#hostel-multiple-insert-room-false").show();
            $("#hostel-multiple-insert-room-true").hide();
        } else {
            console.log('insert single room');
            $("#hostel-multiple-insert-room-false").hide();
            $("#hostel-multiple-insert-room-true").show();
        }
    });


//    $('#deleteAll').click(function () {
//        var keys = $("#entry-inventory").yiiGridView("getSelectedRows").length;
//        if (keys > 0) {
//            if (confirm('are you sure?')) {
//                $.post(
//                        "index.php?r=entry/delete-multiple",
//                        {
//                            pk: $('#entry-inventory').yiiGridView('getSelectedRows')
//                        },
//                        function () {
//                            $.pjax.reload({container: '#entry-inventory'});
//                        }
//                );
//            }
//        } else {
//            alert("No rows selected for download.");
//        }
//
//    });








});